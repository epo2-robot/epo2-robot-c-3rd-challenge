#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <string.h>

#include "wireless.h"
#include "initialize.h"
#include "maze_router.h"

#define COMPORT "COM3"
#define BAUDRATE CBR_9600

void initSio(HANDLE hSerial)
{

    COMMTIMEOUTS timeouts = {0};
    DCB dcbSerialParams = {0};

    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);

    if (!GetCommState(hSerial, &dcbSerialParams))
    {
        printf("error getting state \n");
    }

    dcbSerialParams.BaudRate = BAUDRATE;
    dcbSerialParams.ByteSize = 8;
    dcbSerialParams.StopBits = ONESTOPBIT;
    dcbSerialParams.Parity   = NOPARITY;

    if(!SetCommState(hSerial, &dcbSerialParams))
    {
        printf("error setting state \n");
    }

    timeouts.ReadIntervalTimeout = 50;
    timeouts.ReadTotalTimeoutConstant = 50;
    timeouts.ReadTotalTimeoutMultiplier = 10;

    timeouts.WriteTotalTimeoutConstant = 50;
    timeouts.WriteTotalTimeoutMultiplier = 10;

    if(!SetCommTimeouts(hSerial, &timeouts))
    {
        printf("error setting timeout state \n");
    }
}


char readByte(HANDLE hSerial, char *buffRead)
{

    DWORD dwBytesRead = 0;

    if (!ReadFile(hSerial, buffRead, 1, &dwBytesRead, NULL))
    {
        /* printf("error reading byte from input buffer \n");*/ ;
    }
    if (buffRead[0] != 0 )
        printf("Byte read from read buffer is: %c \n", buffRead[0]);

    return    buffRead[0] ;
}


int writeByte(HANDLE hSerial, char *buffWrite)
{

    DWORD dwBytesWritten = 0;

    if (!WriteFile(hSerial, buffWrite, 1, &dwBytesWritten, NULL))
    {
        printf("error writing byte to output buffer \n");
    }
    printf("Byte written to write buffer is: %c \n", buffWrite[0]);

    return(0);
}

int open_connection()
{
    HANDLE hSerial;
    int i = 0 ;
    int j =0;
    int r = 0; /* route number */
    char *begin = "b" ;
    char *stop = "s" ;
    char start[8] = "c41" ;
    int test ;
    int num_mines=0;

    edges_to_visit* p;

    char byteBuffer[BUFSIZ+1];

    hSerial = CreateFile(COMPORT,
                         GENERIC_READ | GENERIC_WRITE,
                         0,
                         0,
                         OPEN_EXISTING,
                         FILE_ATTRIBUTE_NORMAL,
                         0
                        );

    if(hSerial == INVALID_HANDLE_VALUE)
    {
        if(GetLastError()== ERROR_FILE_NOT_FOUND)
        {
            printf(" serial port does not exist \n");
        }

        printf(" some other error occured. Inform user.\n");
    }


    initSio(hSerial);
    writeByte(hSerial, begin) ;

    while (1)
    {
        while ( 1 )
        {
            if ( (readByte(hSerial, byteBuffer) ) == 'k')
            {

                if (directions_robot[i] == NULL )
                                break ;

                else
                {
                     printf("\n number of mines:  %d   ", num_mines);
                    writeByte(hSerial, directions_robot[i]) ;
                    if ( strncmp( route[i] ,"e",1) ==0  )
                    if (edges_head->next != NULL)
                        adjust_edges(route[i]) ;
                    ++i ;
                    byteBuffer[0] = 0 ;

                }
            }


        if (( readByte(hSerial, byteBuffer)) == 'm'  )
        {

            num_mines++;
            if (num_mines == 13)
            {
                writeByte(hSerial, stop) ;
                 mine(route[i-1] ,route[i-2] , route[i] , r);
                byteBuffer[0] = 0 ;
                break;
            }
            mine(route[i-1] ,route[i-2] , route[i] , r);
            i = 0;
            writeByte(hSerial, directions_robot[i]) ;
            ++i ;
            byteBuffer[0] = 0 ;

        }
        if (( readByte(hSerial, byteBuffer)) == 'x'  )
        {

            writeByte(hSerial, directions_robot[i-1]) ; /* send the commando again */

            byteBuffer[0] = 0 ;
        }

    }

        if (num_mines == 13)
                break;
    j = i;
    i = 0 ;

    find_mines(route[j] , route[j-2] ,  r) ;


}



        if (edges_empty != 1)
        delete_edges_linked_list();


        initialize_edges_route() ;



    for (i=0 ; list_edges[i] != NULL ; ++i)
    adjust_edges(list_edges[i]) ;

    i= 0 ;
    p =edges_head ;

    while(p != NULL)
    {
        ++i;
        printf("\n%s\n" , p->edge);
        p = p->next ;

    }

    printf("\n%d" ,i);


i=0;


strcpy(old_next_location[0] , "1");


 generate_new_route(start ) ;


challenge(start ,check_points[0]) ;

printf("\n number of mines:  %d   ", num_mines);

 printf("\n All mines have been found \n when ready press 1\n");
    scanf("%d" , &test);



writeByte(hSerial, begin) ;


 while (1)
    {
        while ( 1 )
        {
           if ( (readByte(hSerial, byteBuffer) ) == 'k')

              {

                    if (directions_robot[i] == NULL )
                                break ;

                else
                {
                    writeByte(hSerial, directions_robot[i]) ;
                    if (strncmp( route[i] ,"e",1) ==0 )
                         if (edges_head->next != NULL)
                        adjust_edges(route[i]) ;
                    ++i ;
                    byteBuffer[0] = 0 ;

                }
              }



        if (( readByte(hSerial, byteBuffer)) == 'm'  )
        {

                num_mines++;
                writeByte(hSerial, stop) ;
                byteBuffer[0] = 0 ;
                break;

        }
       if (( readByte(hSerial, byteBuffer)) == 'x'  )
        {

            writeByte(hSerial, directions_robot[i-1]) ; /* send the commando again */

            byteBuffer[0] = 0 ;
        }

    }



        if (num_mines == 14)
        break;
    j = i;
    i = 0 ;

    find_mines(route[j] , route[j-2] ,  r) ;

}




printf("ZIGBEE IO DONE!\n");
CloseHandle(hSerial);
return 0;


}
