#ifndef MAZE_ROUTER_H_INCLUDED
#define MAZE_ROUTER_H_INCLUDED




void block_edge(char *name); /* function to block edges */
cell* find_cell( char name[8]); /* function to find the address of a cell */
void find(int k ) ; /*function used to find a location of a cell of value k */
void count(int k) ; /* function to count how many cells have the value k */
int shortest(cell *stationp1,cell *stationp2) ;/*function to find the shortest route */
void LEE(cell* stationp1 , cell* stationp2) ;/* lee algorithm */
void direction_finder(char* route_p , char* route_n , char* route_an); /*converting the route to forward , right and left */
void robot_directions(int i) ;/*converting the route to forward , right and left */
int challenge(char first_checkpoint[8] , char second_checkpoint[8]) ;
void mine(char* mine_location , char* present_location , char* next_location ,int r) ; /* generate a new route when a mine is detected */
void find_mines(char* present_location , char* next_location , int r) ;
void append_path (int i , char d[2]);





#endif
