#ifndef INITIALIZE_H_INCLUDED
#define INITIALIZE_H_INCLUDED

typedef struct cell cell ;

struct cell
{
    int v ;
    char name[8] ;
};

cell maze[13][13] ;


typedef struct edges_to_be_visited      edges_to_visit ;

struct edges_to_be_visited
{
    char  edge[8];
    char  first_cross[8];
    char  second_cross[8];

    edges_to_visit *next ;
};





edges_to_visit *edges_head;





char* route[100]; /* used to save the generated route */
char* directions_robot[100] ; /*used to save the directions of the robot r,l,f */
char* list_edges[20];/*used to save the location of each mine detected */
char* old_next_location[1] ; /* used to save the next location of the old route when a mine is detected*/
char* check_points[1] ;


void initialize_edges_route(void) ;

void adjust_edges(char* route) ;
void delete_edge(edges_to_visit* p2) ;
void generate_new_route(char *start_location ) ;
void delete_edges_linked_list(void);

int edges_empty ;



void initialize_maze();
void initialize_maze_name();
void print_maze() ;
void free_all() ;

#endif
