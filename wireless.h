#ifndef WIRELESS_H_INCLUDED
#define WIRELESS_H_INCLUDED


/*--------------------------------------------------------------
// Function: initSio
// Description: intializes the parameters as Baudrate, Bytesize,
//           Stopbits, Parity and Timeoutparameters of
//           the COM port
//--------------------------------------------------------------*/

void initSio(HANDLE hSerial) ;

/*//--------------------------------------------------------------
// Function: readByte
// Description: reads a single byte from the COM port into
//              buffer buffRead
//--------------------------------------------------------------*/
char readByte(HANDLE hSerial, char *buffRead) ;
/*//--------------------------------------------------------------
// Function: writeByte
// Description: writes a single byte stored in buffRead to
//              the COM port
//-------------------------------------------------------------- */
int writeByte(HANDLE hSerial, char *buffWrite) ;

int open_connection() ;

#endif
