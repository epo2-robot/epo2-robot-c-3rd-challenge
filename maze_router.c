#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "initialize.h"
#include "maze_router.h"



int cnt=0;
int p=0 , q = 0;
int l ;

/*
Function to implement the Lee algorithm
*/
void LEE(cell* stationp1 , cell* stationp2) /* stationp1 is begin point  , stationp2 is target point */
{
    int k=1 ;

    stationp2->v=k ;   /* Mark target cell with value k */

    /*  while begin cell not marked yet with value grater than 0
     mark all neighbor cells of cells marked with k, with k+1,
      if they have a value 0. */

    while(stationp1->v ==0) /*while begin cell is still zero */
    {
        count(k); /*function to count how many cells have the value k and set the value into a global variable cnt */

        while(cnt)
        {
            find(k); /* function to find the location of every cell with value k and set the location into global variables p and q
                        where p is the row value and q is the column value */

            /* add one to all cells next to cell marked with k */
            if(p!=12) /*if the cell marked with value k is not located in the last row */
            {
                if (maze[p+1][q].v == 0) /* if the neighbor cell ( in the next row) has the value 0 */
                    maze[p+1][q].v = k+1 ; /* then set the value of k+1 in the cell */
            }

            if(p!=0)  /*if the cell marked with value k is not located in the first row */
            {
                if (maze[p-1][q].v == 0) /* if the neighbor cell ( in the previous row) has the value 0 */
                    maze[p-1][q].v = k+1 ;
            }
            if(q!=12) /*if the cell marked with value k is not located in the last column */
                if (maze[p][q+1].v == 0)  /* if the neighbor cell ( in the next column) has the value 0 */
                {
                    maze[p][q+1].v = k+1 ;
                }
            if(q!=0) /*if the cell marked with value k is not located in the first column */
            {
                if (maze[p][q-1].v == 0) /* if the neighbor cell ( in the previous column) has the value 0 */
                    maze[p][q-1].v = k+1 ;
            }
            cnt--; /* decrement cnt to go to the next cell that has the value k and repeat the loop */
        }
        /* when the operation is done to all cells that have the value k */
        k = k+1 ;
        p=q=0;

    }
}
/* function to find the cells of value k */
void find(int k )
{
    static int i=0,j=0; /* used to remember where was the last cell of value k */

    if(p==0 && q==0) /* when k has new value then start from the beginning */
        i=j=0;
    if(j>=12)  /* when the last cell of value k was at the end of the row */
        ++i; /* got to the next column */

    for (; i<13 ; ++i)
    {
        if(j>=12) /* when j reaches the cell  at the end of the row */
        {
            j=0 ; /* go to the beginning of the row */
        }
        for ( ; j<13; ++j)
        {
            if (maze[i][j].v == k) /* when the cell of value k is found */
            {
                p = i; /* then  make p and q equal to the location of that cell */
                q = j;
                ++j ; /* increment j to start searching from the next cell to other cell of value k  */
                return ;
            }
        }

        if(i>=12) /* when i reaches  the cell  at the end of the column */
        {
            i=-1; /* go to the next column */
            j=0 ; /* go to the beginning of the row */
        }

    }

}
/*
function to count the number of cells that have the value k
*/
void count(int k)
{
    int i , j;
    for (i=0 ; i<13 ; ++i)
        for (j=0 ; j<13; ++j)
            if (maze[i][j].v == k)
                ++cnt ; /* global variable */

    return ;

}
/*
function to block roads by setting the value -1 in the blocked edge cell
*/
void block_edge(char *name)
{
    int i , j;
    /* find the location of the edge that has to be blocked */
    for (i=0 ; i<13 ; ++i)
        for (j=0 ; j<13; ++j)
            if ( (strcmp(maze[i][j].name,name)) == 0 )
            {
                maze[i][j].v= -1; /* when found set -1 in the edge cell */
                return ;
            }
}
/*
function to find the location of a cell
*/
cell* find_cell(char name[8])
{
    int i , j ;
    for (i=0 ; i<13 ; ++i)
        for (j=0 ; j<13; ++j)
            if ( (strcmp(maze[i][j].name,name)) ==0 )
                return (&maze[i][j]) ; /* when found return the address of the cell */

    return 0;
}
/*
function to find the shortest way from start cell to target cell
*/

int shortest(cell *stationp1,cell *stationp2)
{
    /* Go to the start cell  Trace back */

    static int i=0 ;
    int j = 0 ;
    /* print the  neighbor cell that has a lower value and without printing the edge cells */
   /* if (strncmp( (stationp1)->name ,"e",1)!=0)
        printf("%s ",(stationp1)->name); */
    /* save the route in character pointer array route */
    route[i] = calloc(8 ,  sizeof (char)) ;
    route[i]= (stationp1)->name ;
    ++i ;

    /*while target cell not yet reached
    Go to a neighbor cell that has a lower value. */

    while(stationp1->v != stationp2->v)
    {

        if((stationp1->v > (stationp1+1)->v) && ((stationp1+1)->v != -1) && ((stationp1+1)->v != 0) && strncmp( (stationp1)->name ,"4",1) && strncmp( (stationp1)->name ,"5",1) && strncmp( (stationp1)->name ,"6",1))

            return shortest((stationp1+1) , stationp2);

        else if ((stationp1->v > (stationp1-1)->v) && ((stationp1-1)->v != 0) && ((stationp1-1)->v != -1) && strncmp( (stationp1)->name ,"10",1) && strncmp( (stationp1)->name ,"11",1) && strncmp( (stationp1)->name ,"12",1))

            return  shortest((stationp1-1) , stationp2);

        else if ((stationp1->v > (stationp1-13)->v) && ((stationp1-13)->v != 0) && ((stationp1-13)->v != -1) && strncmp( (stationp1)->name ,"9",1) && strncmp( (stationp1)->name ,"8",1) && strncmp( (stationp1)->name ,"7",1))

            return shortest((stationp1-13) , stationp2);

        else if ((stationp1->v > (stationp1+13)->v) && ((stationp1+13)->v != 0) && ((stationp1+13)->v != -1) && strncmp( (stationp1)->name ,"1",1) && strncmp( (stationp1)->name ,"2",1) && strncmp( (stationp1)->name ,"3",1))

            return shortest((stationp1+13) , stationp2);

    }
    j = i ;
    i = 0;


    return j ; /* return the length of the route */
}

/*
Functions to convert the route to directions: forward, left, right.
*/
/*
This function calls the function direction finder and pass every call
three elements of the route with step of 2 ( without the edges )
*/

void robot_directions(int i)
{
    int j = 0;
    l = 0 ;


        direction_finder(*old_next_location , *(route), *(route+2) );
        for (l=2 , j=0 ; j<i ; l+= 2 , j+= 2)
                direction_finder( *(route+j), *(route+2+j) , *(route+4+j)  ) ;

    return ;
}
/*
this function find the location of the three cells that are passed by robot direction
and then compare the location of the cells to determine the direction of the robot ( right , forward .. )
and each edge is converted to forward
*/

void direction_finder(char* route_p , char* route_n , char* route_an)

{
    int i = 0 , j = 0 ;
    int i1=0 ,i2=0,i3=0,j1=0,j2=0,j3=0 ;


    if (route_p == NULL) /*end of route */
    {
        i1= -1 ;
        j1= -1 ;
    }
    else
    {
        /* find location of the first cell */
        for (i=0 ; i<13 ; ++i)
        {
            for (j=0 ; j<13; ++j)
                if ( (strcmp(maze[i][j].name, (route_p )) ) == 0  )/*when found save it in i1 and j1*/
                {
                    i1 = i ;
                    j1 = j ;
                    break ;
                }
        }
    }

    if (route_n == NULL)
    {
        i2 = -1 ;
        j2 = -1 ;
    }
    else
    {
        /* find location of the second cell */
        for (i=0 ; i<13 ; ++i)
        {
            for (j=0 ; j<13; ++j)
                if ( (strcmp(maze[i][j].name, (route_n ) )) == 0  )/*when found save it in i2 and j2*/
                {
                    i2 = i ;
                    j2 = j ;
                    break ;
                }
        }
    }

    if (route_an == NULL )
    {
        i3 = -1 ;
        j3 = -1 ;
    }
    else
    {
        /* find location of the third cell */
        for (i=0 ; i<13 ; ++i)
        {
            for (j=0 ; j<13; ++j)
                if  ( (strcmp(maze[i][j].name, (route_an) ) )== 0  ) /*when found save it in i3 and j3*/
                {
                    i3 = i ;
                    j3 = j ;
                    break ;
                }
        }
    }
   /* when the finding the location of the three cell is done then compare
       to determine the direction forward, right, left */

    if ( (i1==-1) || (j1==-1)) /*end of the route */
        return ;
    else
    {
        if ( ((j1==j2) && (j3==-1)) || ( (i1==i2) && (i3==-1)  ) )
            return ;
        if ( (((j2==-1) && (j3==-1))) || ((i2==-1) && (i3==-1)) )
            return ;
        if ((j1==j2) && (j2==j3)) /*when all three cells in the same row then forward */
        {
           /* printf("FORWARD ") ; */
            append_path(l , "f") ;
            /* setting the edge to forward  */
            /* at the end of the route we dont need to set forward */

            /* printf("FORWARD ") ; */
                 append_path( l+1 , "f") ; /*edge */

            return ;
        }

        if ( (i1==i2) && (i2==i3)) /*when all three cells in the same column then forward */
        {
           /* printf("FORWARD ") ; */
              append_path( l , "f") ;

           /* printf("FORWARD ") ; */
                 append_path( l+1 , "f") ; /*edge */
            return ;
        }

        if ( (j1==j2) && (j2!=j3) ) /* when the first and the second cell are is the same row and the third is not */
        {

            if(j2>j3 && i1 > i2)
            {
                /*printf("LEFT ") ;*/
               append_path( l , "l") ;


                   /* printf("FORWARD ") ;*/
                     append_path( l+1 , "f") ; /*edge */

                return ;

            }
            if (j2<j3 && i1>i2 )
            {
                /*printf("RIGHT ") ; */
               append_path( l , "r") ;

                    /*printf("FORWARD ") ;*/
                     append_path( l+1 , "f") ;/*edge*/

                return ;
            }
            if(j2>j3 && i1 < i2)
            {
               /* printf("RIGHT ") ; */
               append_path( l , "r") ;
                   /* printf("FORWARD ") ;*/
                   append_path( l+1 , "f") ;

                return ;
            }
            if (j2<j3 && i1< i2 )
            {
                /*printf("LEFT ") ;*/
                 append_path( l , "l") ;
                   /* printf("FORWARD ") ;*/
                   append_path( l+1 , "f") ;

                return ;
            }

        }

        if ( (i1==i2) && (i2!=i3 ) ) /* when the first and the second cell are is the same column and the third is not */
        {

            if(i2<i3 && j1>j2)
            {
                /*printf("LEFT ") ; */
               append_path( l , "l") ;

                   /* printf("FORWARD ") ; */
                   append_path( l+1 , "f") ;

                return ;
            }
            if (i2>i3 && j1>j2)
            {
               /* printf("RIGHT ") ;*/

                append_path( l , "r") ;

                   /* printf("FORWARD ") ; */
                   append_path( l+1 , "f") ;

                return ;
            }
            if(i2>i3 && j1<j2)
            {
                /*printf("LEFT ") ;*/
               append_path( l , "l") ;
                    /*printf("FORWARD ") ;*/
                   append_path( l+1 , "f") ;

                return ;
            }
            if (i2<i3 && j1<j2)
            {
                /*printf("RIGHT ") ;*/
                append_path( l , "r") ;

                    /*printf("FORWARD ") ;*/
                   append_path( l+1 , "f") ;

                return ;
            }
        }
    }
    return ;

}

void append_path (int i , char d[2])
{
    directions_robot[i] = calloc(2 , sizeof(char));
    strcpy( directions_robot[i] , d) ;

    return ;

}

int challenge(char first_checkpoint[8] , char second_checkpoint[8] )
{
    int i=0  ;
    int lenght = 0 ; /*route length */
    cell  *first_checkpoint_location  , *second_checkpoint_location  ;


    initialize_maze();
    /* block all edges that are saved in character pointer array list edges */
    for (i=0 ; list_edges[i] != NULL ; ++i)
        block_edge(list_edges[i]);



    first_checkpoint_location = find_cell(first_checkpoint); /* find location of first check point */
    second_checkpoint_location = find_cell(second_checkpoint); /* find location of target check point */


    LEE( first_checkpoint_location , second_checkpoint_location); /* implement lee algorithm */

    lenght= shortest(first_checkpoint_location,second_checkpoint_location); /* find shortest route */

   /* printf("\n");
    for (j=0; j<lenght; ++j)
        printf("%s ",route[j]);
    printf("\n"); */

    robot_directions(lenght) ; /* convert the route to directions forward, left, right, */

    return lenght ;

}
/*
Function used to generate a new route when a mine is detected
and to save the mine location in char pointer array list_edges
*/

void mine(char *mine_location , char* present_location , char* next_location , int r)
{
    int i=0  ;
    char start_location[8] ;




    /* list_edges is initialized to NULL so we have to go to the first non NULL element to save the mine location */
    for (i=0 ; list_edges[i] != NULL ; ++i) ;
    list_edges[i] = malloc(8* sizeof(char)) ;

    strcpy(list_edges[i],mine_location) ;

    /* when a mine is detected, save the next location of the old route  */

    strcpy(old_next_location[0] , next_location);
    /* when a mine is detected save the present location in start location to generate a new route */
    strcpy(start_location , present_location) ;

    if (edges_head -> next == NULL)
    {
         adjust_edges(list_edges[i]);
         return ;
    }


   generate_new_route(start_location );


        challenge(start_location ,check_points[0]) ; /* generate new route */


    /* because the robot has to ride backwards when a mine is detected
    we have to convert the first commando of the new generated route */

    if ( (strcmp(directions_robot[0],"l" ) ) == 0  )
        strcpy( directions_robot[0] , "r" );
    else if ( (strcmp(directions_robot[0], "r" ) ) == 0  )
        strcpy( directions_robot[0] , "l");
   else if ( (strcmp(directions_robot[0],"f" ) ) == 0  )
         strcpy( directions_robot[0] , "u");




         return;


}

void find_mines(char* present_location , char* next_location , int r)
{
    char start_location[8] ;
    char temp[8];

    strcpy(old_next_location[0] , next_location);
     strcpy(temp, next_location);


    strcpy(start_location , present_location) ;

    generate_new_route(start_location ) ;


        challenge(start_location ,check_points[0]) ;


         if ( (strcmp(temp,route[2] ) ) == 0  )
         {

             if  ( (strcmp(route[0], "c04") == 0 ) && ( strcmp(route[2] , "c14")  ) == 0 )
             strcpy(directions_robot[0] , "U");

            else if ( (strcmp(route[0] , "c44") == 0 ) && ( strcmp(route[2] , "c43")  ) == 0 )
                strcpy(directions_robot[0] , "U");

             else if ( (strcmp(route[0] , "c00") == 0 ) && ( strcmp(route[2] , "c01")  ) ==0 )
                strcpy(directions_robot[0] , "U");

                else if ( (strcmp(route[0] , "c40") == 0 ) && ( strcmp(route[2] , "c30")  ) ==0 )
                strcpy(directions_robot[0] , "U");

                else
                    strcpy(directions_robot[0] , "u");

         }



        return;


}

void adjust_edges(char* route)
{
    edges_to_visit* p;
    p = edges_head ;

    while(p != NULL)
    {

        if ( strcmp(p->edge , route) ==0 )
        {
            delete_edge (p);
            return ;
        }


                p = p->next ;
    }


     return ;

}

void delete_edge(edges_to_visit* p2)
{
    edges_to_visit *temp;

    temp = edges_head ;

    if ( (temp == p2) && ( p2->next != NULL ) ) /* firs element */
    {
       temp = temp -> next ;
        edges_head = temp ;
        p2 -> next = NULL ;
        free(p2);
        return;
    }
    else if ( (p2->next == NULL ) && (temp->next != NULL) ) /*last element */
    {
        while(temp->next != p2 )
            temp = temp->next ;
            temp -> next = NULL ;
            free(p2) ;
            return ;
    }

    else if ( (p2->next == NULL ) && (temp == p2) ) /*one element */
    {

            free(p2) ;
            edges_empty = 1 ;
            return ;
    }

    else if ( p2-> next != NULL)
    {
        while(temp->next != p2 )
            temp = temp->next ;
            temp -> next = p2 -> next ;
            p2 -> next = NULL ;
            free(p2) ;
            return ;

    }


    return ;

}

void generate_new_route(char *start_location)
{
    edges_to_visit* p;
    char* temp;
    int i =0  ;
    int route_1  , route_2 ;

    p = edges_head ;

    while(p != NULL)
    {
      if ((strcmp(p->first_cross, start_location) ) ==0 )
      {
          check_points[0] = p->second_cross ;
            return ;
      }

      else
        p = p->next ;
    }



    p = edges_head ;

    while(p != NULL)
    {
      if ((strcmp(p->second_cross, start_location) ) ==0 )
      {

        check_points[0]  =  p->first_cross ;
        return ;
      }

      else
        p = p->next ;
    }



        p = edges_head ;

        i = 1000 ;

        while(p != NULL)
        {
                    route_1 = challenge(start_location,p->first_cross) ;
                    route_2 = challenge(start_location,p->second_cross) ;

                      if( route_1 <  route_2 )
                      {
                          if (route_1 < i)
                          {
                                i = route_1 ;
                                temp = (p->first_cross) ;
                                if ( i <= 3)
                                    break ;
                          }
                      }
                      else
                      {
                          if ( route_2 < i)
                          {
                                i = route_2 ;
                                temp = (p->second_cross) ;
                                if ( i <= 3)
                                    break ;
                          }
                      }

            p = p->next ;
        }


            check_points[0] = temp ;


        return ;

}
