#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "initialize.h"



void initialize_maze()
{
    int i,j;

    for (i = 0; i < 13; i++)
    {
        for (j = 0; j < 13; j++)
        {
            /* Hekje */
            if (i == 4 || i == 6 || i == 8) maze[i][j].v = 0;
            else if (j == 4 || j == 6 || j == 8) maze[i][j].v = 0;
            /* Vierkant */
            else if (i == 2 && j >= 2 && j <= 10) maze[i][j].v = 0;
            else if (i == 10 && j >= 2 && j <= 10) maze[i][j].v = 0;
            else if (j == 2 && i >= 2 && i <= 10) maze[i][j].v = 0;
            else if (j == 10 && i >= 2 && i <= 10) maze[i][j].v = 0;
            /* -1 */
            else maze[i][j].v = -1;
        }
    }

    for (i=0; i<100 ; ++i)
    {
        free(route[i]) ;
        free(directions_robot[i] ) ;
    }


     for (i=0; i<100 ; ++i)
    {
        (route[i]) = NULL ;
        (directions_robot[i] ) =  NULL ;
    }



    return ;
}

void print_maze()
{
    int i , j ;

    for (i=0 ; i<13 ; ++i)
    {
        for (j=0 ; j<13; ++j)
            printf("%6s ",maze[i][j].name);
        printf("\n");
    }
    printf(("\n\t\t\t"));
    for (i=0 ; i<13 ; ++i)
    {
        for (j=0 ; j<13; ++j)
            printf("%2d ",maze[i][j].v);
        printf("\n\t\t\t");
    }

    return ;
}

void free_all()
{
    int i ;
     for (i=0; i<100 ; ++i)
    {
        free(route[i]) ;
        free(directions_robot[i] ) ;
    }

     for (i=0; i<20 ; ++i)
      free(list_edges[i])  ;

      free(old_next_location[0]);
      free(check_points[0]);


      return ;

}
void initialize_edges_route(void)
{
    edges_to_visit *temp ;

   temp = malloc (sizeof(edges_to_visit)) ;

   edges_head = temp ;

   strcpy(temp->edge,"e4142");
    strcpy(temp->first_cross,"c41");
    strcpy(temp->second_cross,"c42");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp ->next;



    strcpy(temp->edge,"e4243");
    strcpy(temp->first_cross,"c42");
    strcpy(temp->second_cross,"c43");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

    strcpy(temp->edge,"e3343");
    strcpy(temp->first_cross,"c43");
    strcpy(temp->second_cross,"c33");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e3334");
    strcpy(temp->first_cross,"c33");
    strcpy(temp->second_cross,"c34");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e3444");
    strcpy(temp->first_cross,"c34");
    strcpy(temp->second_cross,"c44");
   temp->next = malloc (sizeof(edges_to_visit)) ;
   temp = temp->next ;

    strcpy(temp->edge,"e4344");
    strcpy(temp->first_cross,"c43");
    strcpy(temp->second_cross,"c44");
  temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e3233");
    strcpy(temp->first_cross,"c32");
    strcpy(temp->second_cross,"c33");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e3242");
    strcpy(temp->first_cross,"c32");
    strcpy(temp->second_cross,"c42");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

      strcpy(temp->edge,"e2232");
    strcpy(temp->first_cross,"c22");
    strcpy(temp->second_cross,"c32");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

    strcpy(temp->edge,"e2223");
    strcpy(temp->first_cross,"c22");
    strcpy(temp->second_cross,"c23");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e1323");
    strcpy(temp->first_cross,"c13");
    strcpy(temp->second_cross,"c23");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e1314");
    strcpy(temp->first_cross,"c13");
    strcpy(temp->second_cross,"c14");
  temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e1424");
    strcpy(temp->first_cross,"c14");
    strcpy(temp->second_cross,"c24");
     temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

    strcpy(temp->edge,"e1213");
    strcpy(temp->first_cross,"c12");
    strcpy(temp->second_cross,"c13");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e1222");
    strcpy(temp->first_cross,"c12");
    strcpy(temp->second_cross,"c22");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;


     strcpy(temp->edge,"e2333");
    strcpy(temp->first_cross,"c23");
    strcpy(temp->second_cross,"c33");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e2324");
    strcpy(temp->first_cross,"c23");
    strcpy(temp->second_cross,"c24");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e2434");
    strcpy(temp->first_cross,"c24");
    strcpy(temp->second_cross,"c34");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;



 strcpy(temp->edge,"e0313");
    strcpy(temp->first_cross,"c03");
    strcpy(temp->second_cross,"c13");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;


    strcpy(temp->edge,"e0203");
    strcpy(temp->first_cross,"c02");
    strcpy(temp->second_cross,"c03");
  temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

    strcpy(temp->edge,"e0212");
    strcpy(temp->first_cross,"c02");
    strcpy(temp->second_cross,"c12");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e0304");
    strcpy(temp->first_cross,"c03");
    strcpy(temp->second_cross,"c04");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

    strcpy(temp->edge,"e0414");
    strcpy(temp->first_cross,"c04");
    strcpy(temp->second_cross,"c14");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;


    strcpy(temp->edge,"e0102");
    strcpy(temp->first_cross,"c01");
    strcpy(temp->second_cross,"c02");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;


    strcpy(temp->edge,"e0111");
    strcpy(temp->first_cross,"c01");
    strcpy(temp->second_cross,"c11");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e1112");
    strcpy(temp->first_cross,"c11");
    strcpy(temp->second_cross,"c12");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;



    strcpy(temp->edge,"e1121");
    strcpy(temp->first_cross,"c11");
    strcpy(temp->second_cross,"c21");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e2122");
    strcpy(temp->first_cross,"c21");
    strcpy(temp->second_cross,"c22");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;


    strcpy(temp->edge,"e2131");
    strcpy(temp->first_cross,"c21");
    strcpy(temp->second_cross,"c31");
  temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

     strcpy(temp->edge,"e3132");
    strcpy(temp->first_cross,"c31");
    strcpy(temp->second_cross,"c32");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;


    strcpy(temp->edge,"e3141");
    strcpy(temp->first_cross,"c31");
    strcpy(temp->second_cross,"c41");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;


    strcpy(temp->edge,"e4041");
    strcpy(temp->first_cross,"c40");
    strcpy(temp->second_cross,"c41");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;

    strcpy(temp->edge,"e3040");
    strcpy(temp->first_cross,"c30");
    strcpy(temp->second_cross,"c40");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;


    strcpy(temp->edge,"e3031");
    strcpy(temp->first_cross,"c30");
    strcpy(temp->second_cross,"c31");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;



  strcpy(temp->edge,"e2030");
    strcpy(temp->first_cross,"c20");
    strcpy(temp->second_cross,"c30");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;








    strcpy(temp->edge,"e2021");
    strcpy(temp->first_cross,"c20");
    strcpy(temp->second_cross,"c21");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;


    strcpy(temp->edge,"e1020");
    strcpy(temp->first_cross,"c10");
    strcpy(temp->second_cross,"c20");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;


    strcpy(temp->edge,"e1011");
    strcpy(temp->first_cross,"c10");
    strcpy(temp->second_cross,"c11");
   temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;





    strcpy(temp->edge,"e0010");
    strcpy(temp->first_cross,"c00");
    strcpy(temp->second_cross,"c10");
    temp->next = malloc (sizeof(edges_to_visit)) ;
    temp = temp->next ;


    strcpy(temp->edge,"e0001");
    strcpy(temp->first_cross,"c00");
    strcpy(temp->second_cross,"c01");

    temp -> next  = NULL ;

    return ;

}

void delete_edges_linked_list(void )
{
    edges_to_visit* p;
    p = edges_head ;

   while (p-> next != NULL)
   {

       p = p->next ;
       free(edges_head) ;
       edges_head = p ;
   }

   free(p) ;



}

void initialize_maze_name()
{
      strcpy(maze[0][4].name,"9");
    strcpy(maze[0][6].name,"8");
    strcpy(maze[0][8].name,"7");
    strcpy(maze[4][0].name,"10");
    strcpy(maze[6][0].name,"11");
    strcpy(maze[8][0].name,"12");
    strcpy(maze[12][4].name,"1");
    strcpy(maze[12][6].name,"2");
    strcpy(maze[12][8].name,"3");
    strcpy(maze[4][12].name,"6");
    strcpy(maze[6][12].name,"5");
    strcpy(maze[8][12].name,"4");
    strcpy(maze[4][2].name,"c10");
    strcpy(maze[4][3].name,"e1011");
    strcpy(maze[4][4].name,"c11");
    strcpy(maze[4][5].name,"e1112");
    strcpy(maze[4][6].name,"c12");
    strcpy(maze[4][7].name,"e1213");
    strcpy(maze[4][8].name,"c13");
    strcpy(maze[4][9].name,"e1314");
    strcpy(maze[4][10].name,"c14");

    strcpy(maze[6][2].name,"c20");
    strcpy(maze[6][3].name,"e2021");
    strcpy(maze[6][4].name,"c21");
    strcpy(maze[6][5].name,"e2122");
    strcpy(maze[6][6].name,"c22");
    strcpy(maze[6][7].name,"e2223");
    strcpy(maze[6][8].name,"c23");
    strcpy(maze[6][9].name,"e2324");
    strcpy(maze[6][10].name,"c24");

    strcpy(maze[8][2].name,"c30");
    strcpy(maze[8][3].name,"e3031");
    strcpy(maze[8][4].name,"c31");
    strcpy(maze[8][5].name,"e3132");
    strcpy(maze[8][6].name,"c32");
    strcpy(maze[8][7].name,"e3233");
    strcpy(maze[8][8].name,"c33");
    strcpy(maze[8][9].name,"e3334");
    strcpy(maze[8][10].name,"c34");

    strcpy(maze[2][2].name,"c00");
    strcpy(maze[2][3].name,"e0001");
    strcpy(maze[2][4].name,"c01");
    strcpy(maze[2][5].name,"e0102");
    strcpy(maze[2][6].name,"c02");
    strcpy(maze[2][7].name,"e0203");
    strcpy(maze[2][8].name,"c03");
    strcpy(maze[2][9].name,"e0304");
    strcpy(maze[2][10].name,"c04");

    strcpy(maze[3][2].name,"e0010");
    strcpy(maze[3][4].name,"e0111");
    strcpy(maze[3][6].name,"e0212");
    strcpy(maze[3][8].name,"e0313");
    strcpy(maze[3][10].name,"e0414");

    strcpy(maze[5][2].name,"e1020");
    strcpy(maze[5][4].name,"e1121");
    strcpy(maze[5][6].name,"e1222");
    strcpy(maze[5][8].name,"e1323");
    strcpy(maze[5][10].name,"e1424");

    strcpy(maze[7][2].name,"e2030");
    strcpy(maze[7][4].name,"e2131");
    strcpy(maze[7][6].name,"e2232");
    strcpy(maze[7][8].name,"e2333");
    strcpy(maze[7][10].name,"e2434");

    strcpy(maze[9][2].name,"e3040");
    strcpy(maze[9][4].name,"e3141");
    strcpy(maze[9][6].name,"e3242");
    strcpy(maze[9][8].name,"e3343");
    strcpy(maze[9][10].name,"e3444");

    strcpy(maze[10][2].name,"c40");
    strcpy(maze[10][3].name,"e4041");
    strcpy(maze[10][4].name,"c41");
    strcpy(maze[10][5].name,"e4142");
    strcpy(maze[10][6].name,"c42");
    strcpy(maze[10][7].name,"e4243");
    strcpy(maze[10][8].name,"c43");
    strcpy(maze[10][9].name,"e4344");
    strcpy(maze[10][10].name,"c44");

     check_points[0] = malloc(8* sizeof(char)) ;

    old_next_location[0] = malloc(8* sizeof(char)) ;
   strcpy(old_next_location[0] , "1");

     strcpy(check_points[0] ,"c40") ;

    return ;
}





